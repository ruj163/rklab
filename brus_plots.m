% Brusselator plots
 
close all


% Solver info
solvers = {@solve_expRK32s3,@solve_expRK43s6,@solve_expRK5s10,@solve_MIS_KW3};
snames  = {'MERK32s3','MERK43s6','MERK5s10','MIS-KW3'};
mnames  = {'ERK-3-3','ERK-4-4','Cash-Karp-ERK','Knoth-Wolke-ERK'};
orders  = [3,4,5,3];
colors  = {[0,0.7461,1],[1,0,0],[0.1953,0.8008,0.1953],[1,0,1]};
markers = {'^','<','>','v'};

figure(1);
hold on;
figure(2);
hold on;
figure(3);
hold on;

hdr = 1;

for isol = 1:length(snames)
    sname = snames{isol};
    mname = mnames{isol};
    
   % Open file, read in file
    fprintf('\n____________________________________________________________________\n');
    fprintf('Results for %s ',sname);
    fprintf('\n____________________________________________________________________\n');
    
   % Store variables
    filename    = ['brus_',sname,'_',mname,'.mat'];
    q           = matfile(filename);
    Y           = q.Y;
    Ys          = q.Ys;
    Ytrue       = q.Ytrue;
    err_max     = q.err_max;
    max_rate    = q.max_rate;
    single_err  = q.single_err;
    best_fit    = q.best_fit;
    h           = q.h;
    hfast       = q.hfast;
    time        = q.time;
    nffast      = q.nffast;
    nfslow      = q.nfslow;
    nf          = q.nf;
     
    % Plot Error vs. h
    figure(1)
    hold all
    loglog(hfast,single_err,markers{isol},'color',colors{isol})
    header1{hdr} = mname;
    hold on;
    loglog(h,err_max,[markers{isol},'-'],'color',colors{isol},...
        'MarkerFaceColor',colors{isol})
    header1{hdr+1} = sname;
    hold on; 

    figure(2)
    hold all   
    loglog(nfslow+nffast,err_max,[markers{isol},'-'],'color',colors{isol},...
        'MarkerFaceColor',colors{isol},'LineWidth',2);
    header2{isol} = sname;
    hold on ;
            
    % Plot Error vs. Slow function calls
    figure(3)
    hold all   
    loglog(nfslow,err_max,[markers{isol},'-'],'color',colors{isol},...
        'MarkerFaceColor',colors{isol},'LineWidth',2);
    header3{isol} = sname;
    hold on 
    hdr = hdr + 2;   
    
end

% figure 1 processing
figure(1)
hold off
set(gca,'XScale','log','YScale','log','Fontsize',12);  
set(gca,'xdir','reverse');
xlabel('H')
ylabel('Max Error')
legend(header1,'Location','bestoutside')
plotname = fullfile('C:\Users\Rujeko\Dropbox\MTSalgorithms\numericalresults\figs\brus_convergence');
figname  = 'brus_convergence';
savefig(figname)
print('-depsc',plotname)

% figure 2 processing
figure(2)
hold off
set(gca,'XScale','log','YScale','log','Fontsize',14);      
xlabel('Total function calls')
ylabel('Max Error')
legend(header2,'Location','BestOutside');
plotname = fullfile('C:\Users\Rujeko\Dropbox\MTSalgorithms\numericalresults\figs\brus_total');
figname = 'brus_total';
savefig(figname)
print('-depsc',plotname);
        
% figure 3 processing
figure(3)
hold off
set(gca,'XScale','log','YScale','log','Fontsize',14);      
xlabel('Slow function calls')
ylabel('Max Error')
legend(header2,'Location','bestoutside');
plotname = fullfile('C:\Users\Rujeko\Dropbox\MTSalgorithms\numericalresults\figs\brus_slow');
figname = 'brus_slow';
savefig(figname)
print('-depsc',plotname);