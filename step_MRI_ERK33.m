function [Y,m] = step_MRI_ERK33(fs,ff,t0,Y0,Bi,hs,hf)
% usage: [Y,m] = step_MRI_ERK33(fs,ff,t0,Y0,Bi,hs,hf)
%
% This routine performs a single step of the multirate
% infinitesimal GARK 3rd order with 3 stages (MRK-GARK-ERK33) method 
% for the vector-valued ODE problem
%     y' = fs(t,Y) + ff(t,Y), t >= t0, y in R^n,
%     Y0 = [y1(t0), y2(t0), ..., yn(t0)]'.
% We perform the solve using the 'standard' approach described by
% Knoth & Wolke (1998), i.e., we do NOT consider the problem in
% GARK form.
%
% Inputs:
%     fs     = function handle for (slow) ODE RHS
%     ff     = function handle for (fast) ODE RHS
%     t0     = value of independent variable at start of step
%     Y0     = solution vector at start of step (column vector of length n)
%     Bi     = Butcher table for a single step of an 'inner' (fast) method
%              (can be ERK, DIRK or IRK)
%                 Bi = [ci Ai;
%                       qi bi;
%                       pi di ]
%              Here, ci is a vector of stage time fractions (si-by-1),
%              Ai is a matrix of Butcher coefficients (si-by-si),
%              qi is an integer denoting the method order of accuracy
%              bi is a vector of solution weights (1-by-si),
%              pi is an integer denoting the embedding order of accuracy,
%              di is a vector of embedding weights (1-by-si),
%              The [pi, di] row is optional, and is unused in this
%              routine.  Also, the qi value is unused here.
%     hs     = step size to use for slow time scale
%     hf     = desired step size to use for fast time scale,
%                  hf <= hs*min_{i}(co(i+1)-co(i))
%              Note: this is only a target step size; in fact we
%              will determine each substepping interval and find
%              hinner <= hi such that we take an integer number of
%              steps to subcycle up to each outer stage time.
%
% Outputs:
%     Y      = [y1(t0+hs), y2(t0+hs), ..., yn(t0+hs)].
%     m      = actual number of substeps used for inner method.
%
% Rujeko
% Department of Mathematics
% Southern Methodist University
% September 2018

% dummy stability function, tolerances
estab = @(t,y) inf;
rtol  = 1e20;
atol  = 1e20;

% Define Bo ERK Butcher table for 'outer' (slow) method
Bo = [ 0 , 0 , 0 , 0 ;...
    1/3 , 1/3 , 0 , 0 ;...
    2/3 , 0 , 2/3 , 0 ;...
    1 , 1/4 , 0 , 3/4 ;...
    1 , 1/12 , 1/3 , 7/12];

% Define gamma matrices
Gzero = [1/3 , 0 , 0 ; ...
    -1/3 , 2/3 , 0 ; ...
    0 , -2/3 , 1 ; ...
    1/12 , -1/3 , 7/12 ];

Gone = [ 0 , 0 , 0 ; ...
    0 , 0 , 0 ; ...
    1/2 , 0 , -1/2 ; ...
    0 , 0 , 0 ];
    

% extract ERK method information from Bo, and ensure that it is legal
[Brows, Bcols] = size(Bo);
so = Bcols - 1;           % number of stages
co = Bo(1:so+1,1);          % stage time fraction array
Ao = Bo(1:so,2:so+1);     % ERK coefficients
if (max(max(abs(triu(Ao)))) > 0)
   error('Error: Bo does not specify an explicit RK method table')
end

% initialize outputs
m = 0;
n = length(Y0);
Y = reshape(Y0,n,1);

% initialize temporary variables
Fs = zeros(n,so);
fscale = cell(so,1);

% iterate over remaining outer stages
for i=1:so
   v0 = Y;
   Ti = t0 + co(i)*hs;
   Fs(:,i) = fs(Ti,Y);
   
   % determine 'inner' ODE for this stage
   for j=1:i
      fscale{j} = @(theta) Gzero(i,j) + Gone(i,j)*theta/hs;
   end
   
   for j = i+1:so
       fscale{j} = @ (theta) 0;
   end
   
   if i<so
    dci = co(i+1)-co(i);
   elseif i == so
     dci = 1-co(i);
   else
       dci = 0;
   end
   
   vi = @(theta,v) dci*ff(Ti + (dci*theta),v) + intermediate_fn(Fs,fscale,theta); 
   
   % time interval
   tspan = [0,hs];
   % num internal time steps
   ni = ceil(hs/hf);
   %   step size
   hi = hs/ni;

   % call inner ERK method solver 
   [tvals, V, mi, ~] = solve_ERK(vi, estab, tspan,v0, Bi, rtol, atol, hi, hi, hi);
 
   m = m + mi;

   % update slow 'solution' as result from fast solve
   Y = V(:,end);
end
end
% end of function
function vec = intermediate_fn(Fs,fscale,theta)
    [p,q] = size(Fs);
    vec = zeros(p,1);

    for i = 1:p
      for k = 1:length(fscale)
          vec(i)= vec(i) + Fs(i,k)*fscale{k}(theta);
      end
    end
end