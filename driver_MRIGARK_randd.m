% USAGE : driver_MRIGARK_randd
% driver for reaction diffusion test problem:  
%      u' = 1e-4/ep*u_xx + u^2(1-u)
% where u_x(0,t) = u_x(L,t) and u(x,0) = (1 + exp(lambda*(x-1))^(-1), 
% with parameters lambda = .5*sqrt(2*ep*1e4), L = 3.  
% We evaluate over the time interval [0,3]. 
% A multiple time stepping method is used. 
%
% Rujeko Chinomona
% Department of Mathematics
% Southern Methodist University
% September 2018               

clear, close all

% Set problem parameters
a      = 0; 
b      = 5; 
ns     = 1000;                              % number of spatial grid points
x      = linspace(a,b,ns+1);
ep     = 1e-2;

A    = A_randd(a,b,ns,ep);                        % stiff matrix 
gn   = @(t,y) y.^2 - y.^3;                        % non-stiff part 
ff   = @(t,y) A*y;
fs   = @(t,y) gn(t,y);
fn   = @(t,y) A*y + gn(t,y);
Jff  = @(t,y) A + diag(2*y-3*y.^2);
Y0   = Y0_randd(x,ep);                            %initial condition

% Read in "true" solution
filename = 'randd_ref_soln.mat';
q        = matfile(filename);
Ytrue    = q.Yerk;

% Set time parameters for MTS algorithm
Ti      = 0;
Tf      = 3;                                       % end time 
n       = 31;
tout    = linspace(Ti,Tf,n);                       % intermediate times for solution

% Set up time step
hmax  = tout(2)-tout(1);                           % largest time step
h     = hmax*0.5.^(0:5);                           % large time steps
hfast = 0.001;

for i = length(h):-1:1
    if h(i)/hfast < 4
        h = h(1:i-1);
    end
end

% Set up m values 
mvals = ceil(h/hfast);

% Initialize problem variables/ allocate space
Y          = zeros(ns+1,n);
Y(:,1)     = Y0;
err_max    = zeros(1,length(h));
max_rate   = zeros(1,length(h)-1);
time       = zeros(1,length(h));

% Solver info
solvers = {@solve_MIS_KW3,@solve_MRI_ERK33,@solve_MRI_ERK33b,@solve_MRI_ERK45a};
snames  = {'MIS-KW3','MRI-ERK33','MRI-ERK33b','MRI-ERK45a'};
mnames  = {'Knoth-Wolke-ERK','Knoth-Wolke-ERK','Knoth-Wolke-ERK','ERK-4-4'};
orders  = [3,3,3,4];

for isol = 1:length(solvers)
    solver = solvers{isol};
    sname  = snames{isol};
    mname  = mnames{isol};
    order  = orders(isol);
    
    fprintf('\nRunning convergence test with %s integrator (theoretical order %i)\n',...
            sname,order)
    B = butcher(mname);  s = numel(B(1,:))-1;
    fprintf('\nRunning with inner ERK integrator: %s (order = %i)\n',mname,B(s+1,1))

    % Reset function calls
    nffast     = zeros(1,length(h));
    nfslow     = zeros(1,length(h));

    % Compute solution using MRI_GARK solver
    for j = 1:length(h)
        m = mvals(j);            % for each h, there's an m

        tstart = tic;
        [t,Yout,fastcalls,slowcalls] = solver(fs, ff, tout, Y0, h(j), hfast);
        telapsed = toc(tstart);

        time(j) = telapsed; 
        Y = Yout;
        nffast(j) = fastcalls;
        nfslow(j) = slowcalls;

        % Function calls 
        fprintf('nffast = %g ,  nfslow = %g .\n',nffast(j),nfslow(j));

        % Error calculation
        err_max(j) = max(max(abs(Y - Ytrue)));
        fprintf('Accuracy/Work Results, h = %g  , m = %i:\n',h(j),m)
        fprintf('   maxerr = %.5e,    ',err_max(j));

        % Rate of convergence calculation
        if j > 1
            max_rate(j-1) = log(err_max(j-1)/err_max(j))/log(h(j-1)/h(j));
            fprintf('   maxrate = %.5e \n',max_rate(j-1));  
        end
        fprintf('Time elapsed = %g.\n',telapsed);       
    end

    % Best-fit convergence rate
    p = polyfit(log(h),log(err_max),1);
    fprintf('best-fit convergence rate = %g\n', p(1));

%     % Store variables
%     filename      = ['randdmri_',sname,'_',mname,'.mat'];
%     q             = matfile(filename,'Writable',true);
%     q.Y           = Y;
%     q.Ytrue       = Ytrue;
%     q.err_max     = err_max;
%     q.max_rate    = max_rate;
%     q.best_fit    = p(1);
%     q.h           = h;
%     q.hfast       = hfast;
%     q.time        = time;
%     q.nffast      = nffast;
%     q.nfslow      = nfslow;
end




