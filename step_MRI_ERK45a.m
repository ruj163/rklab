function [Y,m] = step_MRI_ERK45a(fs,ff,t0,Y0,Bi,hs,hf)
% usage: [Y,m] = step_MRI_ERK45a(fs,ff,t0,Y0,Bi,hs,hf)
%
% This routine performs a single step of the multirate
% infinitesimal GARK 4th order with 5 stages (MRK-GARK-ERK45a) method 
% for the vector-valued ODE problem
%     y' = fs(t,Y) + ff(t,Y), t >= t0, y in R^n,
%     Y0 = [y1(t0), y2(t0), ..., yn(t0)]'.
% We perform the solve using the 'standard' approach described by
% Knoth & Wolke (1998), i.e., we do NOT consider the problem in
% GARK form.
%
% Inputs:
%     fs     = function handle for (slow) ODE RHS
%     ff     = function handle for (fast) ODE RHS
%     t0     = value of independent variable at start of step
%     Y0     = solution vector at start of step (column vector of length n)
%     Bi     = Butcher table for a single step of an 'inner' (fast) method
%              (can be ERK, DIRK or IRK)
%                 Bi = [ci Ai;
%                       qi bi;
%                       pi di ]
%              Here, ci is a vector of stage time fractions (si-by-1),
%              Ai is a matrix of Butcher coefficients (si-by-si),
%              qi is an integer denoting the method order of accuracy
%              bi is a vector of solution weights (1-by-si),
%              pi is an integer denoting the embedding order of accuracy,
%              di is a vector of embedding weights (1-by-si),
%              The [pi, di] row is optional, and is unused in this
%              routine.  Also, the qi value is unused here.
%     hs     = step size to use for slow time scale
%     hf     = desired step size to use for fast time scale,
%                  hf <= hs*min_{i}(co(i+1)-co(i))
%              Note: this is only a target step size; in fact we
%              will determine each substepping interval and find
%              hinner <= hi such that we take an integer number of
%              steps to subcycle up to each outer stage time.
%
% Outputs:
%     Y      = [y1(t0+hs), y2(t0+hs), ..., yn(t0+hs)].
%     m      = actual number of substeps used for inner method.
%
% Rujeko Chinomona (based on code by D. Reynolds step_MIS)
% Department of Mathematics
% Southern Methodist University
% September 2018

% dummy stability function, tolerances
estab = @(t,y) inf;
rtol  = 1e20;
atol  = 1e20;

% Define Bo ERK Butcher table for base (slow) method
Bo = [ 0 , 0 , 0 , 0 , 0 , 0 ; ...
    1/5 , 1/5 , 0 , 0 , 0 , 0 ; ...
    2/5 , 1/32, 59/160, 0 , 0 , 0 ; ...
    3/5 , -1/2 , 171/64 , -503/320 , 0 , 0 ; ...
    4/5 , 125773/379760 , -183399/379760 , 175277/189880 , 136/4747 , 0 ; ...
    1 , 1/32 , 1/3 , 11/48 , -1/12 , 47/96 ; ...
    1 , 1/8 , 6403/71670 , 28571/71670 , -4681/71670 , 129673/286680 ];

% Define gamma matrices
Gzero = [1/5 , 0 , 0 , 0 ,0; ...
    -53/16 , 281/80 , 0 , 0 , 0; ...
    -36562993/71394880 , 34903117/17848720 , -88770499/71394880, 0 , 0; ...
    -7631593/71394880 , -166232021/35697440 , 6068517/1519040 , 8644289/8924360 , 0; ...
    277061/303808 , -209323/1139280 , -1360217/1139280 , -148789/56964 , 147889/45120; ...
    -1482837/759520 , 175781/71205 , -790577/1139280 , -6379/56964 , 47/96];

Gone = [ 0 , 0 , 0 , 0 , 0 ; ...
    503/80 , -503/80 , 0 , 0 , 0 ; ...
    -1365537/35697440 , 4963773/7139488 , -1465833/2231090 , 0 , 0 ; ...
    66974357/35697440 , 21445367/7139488 , -3 , -8388609/4462180 , 0 ; ...
    -18227/7520 , 2 , 1 , 5 , -41933/7520 ; ...
    6213/1880 , -6213/1880 , 0 , 0 , 0 ];

% extract ERK method information from Bo, and ensure that it is legal
[Brows, Bcols] = size(Bo);
so = Bcols - 1;           % number of stages
co = Bo(1:so+1,1);          % stage time fraction array
Ao = Bo(1:so,2:so+1);     % ERK coefficients
if (max(max(abs(triu(Ao)))) > 0)
   error('Error: Bo does not specify an explicit RK method table')
end

% initialize outputs
m = 0;
n = length(Y0);
Y = reshape(Y0,n,1);

% initialize temporary variables
Fs = zeros(n,so);
fscale = cell(so,1);

% iterate over outer stages
for i=1:so
   v0 = Y;
   Ti = t0 + co(i)*hs;
   Fs(:,i) = fs(Ti,Y);
   
   % Set increments between consecutive stages of base method
   if i<so
    dci = co(i+1)-co(i);
   elseif i == so
     dci = 1 - co(i);
   else
       dci = 0;
   end
   
   % Determine slow tendency coefficients 
   for j=1:i
      fscale{j} = @(theta) Gzero(i,j) + Gone(i,j)*theta/hs;
   end
   for j = i+1:so
       fscale{j} = @ (theta) 0;
   end
   
   % determine 'inner' ODE for this stage
   vi = @(theta,v) dci*ff(Ti + (dci*theta),v) + intermediate_fn(Fs,fscale,theta); 
   
   % time interval
   tspan = [0,hs];
   % number of  internal time steps
   ni = ceil(hs/hf);
   % step size
   hi = hs/ni;

   % call inner ERK method solver 
   [tvals, V, mi, ~] = solve_ERK(vi, estab, tspan,v0, Bi, rtol, atol, hi, hi, hi);
 
   m = m + mi;

   % update slow 'solution' as result from fast solve
   Y = V(:,end);
end
end
% end of function

function vec = intermediate_fn(Fs,fscale,theta)
    % Computes the linear combination of slow function values
    
    [p,q] = size(Fs);
    vec = zeros(p,1);

    for i = 1:p
      for k = 1:length(fscale)
          vec(i)= vec(i) + Fs(i,k)*fscale{k}(theta);
      end
    end
end